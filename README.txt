The Workflow Wordfilter module enables users of both the workflow_ng and wordfilter modules to combine the power of the two.  You will now be able to use workflow actions on nodes and comments that have been submitted with words defined in the wordfilter filter list.

This module also provides hook_workflow_wordfilter($op, $obj) that will fire when a node or comment has been submitted with words found in the wordfilter list.  This will allow other modules to tap into this process.  $op is either 'node' or 'comment' depending on the type of content submitted and $obj is either the node object or comment object submitted that was found to have words in the filter list.

NOTES: Module will only work on PHP 5.x.

Installation Steps:

1. Install workflow_ng.module (including the workflow UI module)
2. Install wordfilter.module
3. Setup words to be filtered at www.example.com/admin/settings/wordfilter/
4. Install workflow_wordfilter.module (this module)
5. 'Add a new configuration' at www.example.com/admin/build/workflow-ng
6. Choose an event listed under 'Workflow-Wordfilter'
7. Select add an action
8. Choose an action to take place when the event you selected happens.

Voila!


Module developed by Blake Lucchesi (www.boldsource.com) and Robert Douglass (www.robshouse.net)